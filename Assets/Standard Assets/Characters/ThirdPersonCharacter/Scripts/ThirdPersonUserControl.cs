using System;
using TMPro;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
        private Animator m_Animator;

        public GameObject FireBall;
        public GameObject Emiter;

        public float Mana=0;
        //public Timer timer;
        
        //added variables
        [SerializeField] private TMP_Text coinsText;
        [SerializeField] private TMP_Text heartText;
        [SerializeField] private TMP_Text redCrystalText;
        [SerializeField] private TMP_Text blueCrystalText;
        [SerializeField] private TMP_Text greenKeyText;
        [SerializeField] private TMP_Text orangeKeyText;
        private int coinsAmount = 0;
        private int keyOrangeAmount = 0;
        private int keyGreenAmount = 0;
        private int redAmount = 0;
        private int blueAmount = 0;
        private int greenKey = 0;
        private int orangeKey = 0;
        
        public AudioClip coinClip;
        public AudioClip hourglassClip;
        public AudioClip pointsDecrease;
        public AudioSource itemAudioSource;
        
        private void Start()
        {
            m_Animator = GetComponent<Animator>();
            // get the transform of the main camera
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

            // get the third person character ( this should never be null due to require component )
            m_Character = GetComponent<ThirdPersonCharacter>();
        }


        private void Update()
        {
            if(Mana < 4)
            {
                Mana = +Time.time;
            }
            else if (Input.GetMouseButton(0))
            {
                Mana = 0;
                m_Animator.Play("CastSpell");
                Invoke("CreateFireball", 0.8f);
            }
            if (!m_Jump)
            {
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
        }


        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // read inputs
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            bool crouch = Input.GetKey(KeyCode.C);

            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v*m_CamForward + h*m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = v*Vector3.forward + h*Vector3.right;
            }
#if !MOBILE_INPUT
			// walk speed multiplier
	        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif

            // pass all parameters to the character control script
            m_Character.Move(m_Move, crouch, m_Jump);
            m_Jump = false;
        }

        private void CreateFireball()
        {
            Instantiate(FireBall, Emiter.transform.transform);
        }
        
        void OnTriggerEnter(Collider other)
        {
        	if (other.gameObject.tag == "coin")
        	{
        		Destroy(other.gameObject);
        		coinsAmount += 1;
        		coinsText.text = coinsAmount.ToString();
        		itemAudioSource.PlayOneShot(coinClip);
                      
        	}
            else if (other.gameObject.tag == "gem_red")
            {
                Destroy(other.gameObject);
                redAmount += 5;
                redCrystalText.text = redAmount.ToString();
                itemAudioSource.PlayOneShot(coinClip);

            }
            else if (other.gameObject.tag == "gem_blue")
            {
                Destroy(other.gameObject);
                blueAmount += 10;
                blueCrystalText.text = blueAmount.ToString();
                itemAudioSource.PlayOneShot(coinClip);

            }
            else if (other.gameObject.tag == "keygreen")
            {
                Destroy(other.gameObject);
                keyGreenAmount += 1;
                greenKeyText.text = keyGreenAmount.ToString();
                itemAudioSource.PlayOneShot(coinClip);
            }
            else if (other.gameObject.tag == "keyorange")
            {
                Destroy(other.gameObject);
                keyOrangeAmount += 1;
                orangeKeyText.text = keyOrangeAmount.ToString();
                itemAudioSource.PlayOneShot(coinClip);
            }
            
            else if (other.gameObject.tag == "hourglass")
            {
                //timer.AddOrSubstractTime(other.gameObject.GetComponent<Hourglass>().time);
                Destroy(other.gameObject);
                itemAudioSource.PlayOneShot(hourglassClip);

            }
            else if (other.gameObject.tag == "points_remover")
            {
                Destroy(other.gameObject);
                coinsAmount -= coinsAmount < 5 ? coinsAmount : 5;
                coinsText.text = coinsAmount.ToString();
                itemAudioSource.PlayOneShot(pointsDecrease);
            }
        }
    }
}
