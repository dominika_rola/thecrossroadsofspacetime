﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Hourglass : MonoBehaviour
{
    public float time;
    public Material plusMaterial;
    public Material minusMaterial;
    public ParticleSystem ps;
    // Start is called before the first frame update
    void Start()
    {
        if(time >= 0)
        {
            ps.GetComponent<Renderer>().material = plusMaterial;
        } else
        {
            ps.GetComponent<Renderer>().material = minusMaterial;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
