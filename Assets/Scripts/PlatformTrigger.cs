﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformTrigger : MonoBehaviour
{
    public Platform movingPlatform;
    
    void OnTriggerEnter(Collider other)
    {
      movingPlatform.NextPlatform();  
    }
    
}
