﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamekit3D;

public class DoorScript : MonoBehaviour
{
    private bool open;
    public bool KeyOrange;
    public bool KeyGreen;
    private Animation animation;

    void Start()
    {
        animation = GetComponent<Animation>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !open)
        {
            Damageable player = other.GetComponent<Damageable>();
            if(KeyOrange && player.keyOrangeAmount > 0)
            {
                animation.Play();
                open = true;
                player.keyOrangeAmount--;
            }

            if (KeyGreen && player.keyGreenAmount > 0)
            {
                animation.Play();
                open = true;
                player.keyGreenAmount--;
            }
        }
    }

}

