﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private AudioSource mySounds;

    [SerializeField] private AudioClip hoverSounds;

    [SerializeField] private AudioClip clickSound;
    
    
    public void PlayGame()
    {
        SceneManager.LoadScene("Game");
    }
    public void Options()
    {
        //SceneManager.LoadScene("Game");
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    public void HoverSound()
    {
        mySounds.PlayOneShot(hoverSounds);
    }

    public void ClickSound()
    {
        mySounds.PlayOneShot(clickSound);
    }
}
