﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


    public class Timer : MonoBehaviour
    {
        public TextMeshProUGUI TimerText;
        public int SecondCount;
        public int RunningOutSeconds;

        //private float t;
        private float _startTime;
        private bool _ended;
        private bool _runningOut;

        void Start()
        {
            _startTime = Time.time + SecondCount;
            _ended = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (_ended)
                return;
            float t = _startTime - Time.time;

            if (!_runningOut && t <= RunningOutSeconds)
            {
                TimerText.color = new Color32(255, 255, 0, 255);
                _runningOut = true;
            }

            if (t <= 0)
                End();

            string min = ((int)t / 60).ToString();
            string second = (t % 60).ToString("f0");
            if (second.Length < 2)
                second = "0" + second;
            TimerText.text = min + ":" + second;
        }

        private void End()
        {
            TimerText.color = new Color32(255, 0, 0, 255);
            _ended = true;
        }

        public void AddOrSubstractTime(float time)
        {
            Debug.Log(time);
            _startTime += time;
        }
    }
